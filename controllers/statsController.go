package controllers

import (
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)


func TopProducts() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")

		date := c.Query("date")
		period := c.Query("period")

		req, err := http.NewRequest("GET", "http://order-srv:3000/api/orders/stats/topProducts" + "?date=" + date + "&period=" + period, nil)
		if err != nil {
			panic(err)
		}

		req.Header.Set("Authorization", token)
		client := &http.Client{}
		resp, err := client.Do(req)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		sb := string(body)

		c.JSON(http.StatusOK, gin.H{
			"products": sb,
		})
	}
}

func Turnover() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")

		date := c.Query("date")
		period := c.Query("period")

		req, err := http.NewRequest("GET", "http://order-srv:3000/api/orders/stats/turnover" + "?date=" + date + "&period=" + period, nil)
		if err != nil {
			panic(err)
		}

		req.Header.Set("Authorization", token)
		client := &http.Client{}
		resp, err := client.Do(req)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		sb := string(body)

		c.JSON(http.StatusOK, gin.H{
			"turnover": sb,
		})
	}
}

func OrdersCancelled() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")

		req, err := http.NewRequest("GET", "http://order-srv:3000/api/orders/stats/cancelled", nil)
		if err != nil {
			panic(err)
		}

		req.Header.Set("Authorization", token)
		client := &http.Client{}
		resp, err := client.Do(req)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		sb := string(body)

		c.JSON(http.StatusOK, gin.H{
			"nbOrders": sb,
		})
	}
}

func CountDeliveries() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")

		req, err := http.NewRequest("GET", "http://delivery-srv:3000/api/deliveries/stats", nil)
		if err != nil {
			panic(err)
		}

		req.Header.Set("Authorization", token)
		client := &http.Client{}
		resp, err := client.Do(req)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		sb := string(body)

		c.JSON(http.StatusOK, gin.H{
			"nbDeliveries": sb,
		})
	}
}
