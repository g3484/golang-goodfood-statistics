package controllers

import "github.com/gin-gonic/gin"

func HealthCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.IndentedJSON(200, gin.H{"message": "K8S is healthy!"})
	}
}

