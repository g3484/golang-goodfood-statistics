package middleware

import (
	"net/http"
	"strings"

	helpers "statistics/helpers"

	"github.com/gin-gonic/gin"
)

// Authenticate is a middleware that checks if the request has a valid JWT token
func Authenticate (next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if authHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"message": "No token provided"})
			c.Abort()
			return
		}

		tokenString := strings.Split(authHeader, " ")[1]

		claims, err := helpers.ValidateToken(tokenString)

		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"message": "Invalid token"})
			c.Abort()
			return
		}

		c.Set("user", claims)
		next(c)
	}
}
