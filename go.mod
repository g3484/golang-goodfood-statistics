module statistics

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.8.1
	github.com/golang-jwt/jwt/v4 v4.4.2
)
