package main

import (
	"net/http"
	"os"
	routes "statistics/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	if(os.Getenv("JWT_KEY") == "") {
		panic("JWT_KEY not set")
	}

	router := gin.New()
	router.Use(gin.Logger())

	routes.HealthCheck(router)
	routes.StatsRoutes(router)

	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not found"})
	})

	router.Run(":3000")
}
	
