package routes

import (
	controller "statistics/controllers"

	"github.com/gin-gonic/gin"
)

func HealthCheck(incomingRoutes *gin.Engine) {
	incomingRoutes.GET("/api/statistics/health", controller.HealthCheck())
}

