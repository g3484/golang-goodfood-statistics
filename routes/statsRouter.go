package routes

import (
	controller "statistics/controllers"
	"statistics/middleware"

	"github.com/gin-gonic/gin"
)

func StatsRoutes(incomingRoutes *gin.Engine) {
	incomingRoutes.GET("/api/statistics/topProduct", middleware.Authenticate(controller.TopProducts()))
	incomingRoutes.GET("/api/statistics/turnover", middleware.Authenticate(controller.Turnover()))
	incomingRoutes.GET("/api/statistics/cancelled", middleware.Authenticate(controller.OrdersCancelled()))
	incomingRoutes.GET("/api/statistics/deliveries", middleware.Authenticate(controller.CountDeliveries()))
}

