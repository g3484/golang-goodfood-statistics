package helpers

import (
	"os"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
)

// JwtClaim adds email as a claim to the token
type JwtClaim struct {
	Id string
	Email string
	Nbf int64
	Exp int64
	Iat int64
	jwt.StandardClaims
}

func ValidateToken(signedToken string) (claims *JwtClaim, err error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JwtClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(
				os.Getenv("JWT_KEY"),
			), nil
		},
	)

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*JwtClaim)

	if !ok {
		return nil, err
	}

	if claims.Exp < time.Now().Local().Unix() {
		return nil, err
	}

	return claims, nil
}